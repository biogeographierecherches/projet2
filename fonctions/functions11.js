var readline = require("readline-sync");

var capitalEmprunt = readline.questionInt("Combien voulez-vous emprunter? ");
var tauxInteret = readline.questionInt("Quel est le taux propose par votre banquier (par an)");
var dureePret= readline.questionInt("Quelle est la duree de l'emprunt en annee");

console.log("-----------------------------------");

console.log("Votre mensualite sera de %d € par mois ", calculMensualite(capitalEmprunt, tauxInteret));

function calculMensualite(capital, taux, duree){

    var i = taux/100/12;
    var n = duree * 12;
    var mensualite = capital * (i/(1/(1-Math.pow(11+i, -n))));
    return Math.round(mensualite * 100)/100;
}